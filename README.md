# Training - Shell

Get familiar with Linux and the command line on your very first day. (You'll move to Python tomorrow!)

---
## Intro
### Welcome
Welcome to your new home for the next two weeks!

Today is about getting comfortable with Linux and the command line, before your first day of Python tomorrow. It's also about getting used to the working environment at Intek: how to submit a file for correction, how to launch a Sentinel, how to ask for help... Once you get those basics right, you'll be more comfortable to start Python tomorrow.

Today, like every day of the first week, you have a series of exercises to do. Those are training exercices, they are there for you to play around with the concepts and gradually understand how programming works.

People with different backgrounds won't be able to do the same number of exercices each day. You will find on the project page realistic goals according to your initial level: beginner, intermediate or expert. Just go as far as you can and enjoy the learning process!

### How to get corrected?
The guardian of knowledge at Intek is called the Sentinel.

The Sentinel will automatically correct your work at the end of the day and whenever you click on the button to "Launch a Sentinel."

But for the Sentinel to be able to correct you, your work must be available on your Git repository, and with the proper folder and file names. There's no way around it: you MUST respect strictly the name of folders and files for the Sentinel to feedback your work.

So submit each exercice in its own folder named after the exercise, on the Git repository indicated on the project page.

### How to work at Intek?
Intek is a collaborative environment. You are allowed - and strongly encouraged - to talk with other students.

You won't make it through the end of the Hyperspace if you work alone. You need to talk with other people to overcome challenges!

Of course, you still need to understand the work you submit. Don't cheat by submitting work you don't understand.

Also, the mentors are not there to give you the answers (the point is for you to solve the problems), but if you feel lost or overwhelmed, reach out! We are there to support you. :)

### How your day should start
The start of your day, and the submission of the first exercice, hello_world, should look like this:
```sh
# First, you clone the repository given on the project page. 
$ git clone http://gitlab-students.int.intek.edu.vn/training-day-00/laurie day00
Cloning into 'day00'...
Username for 'http://gitlab-students.int.intek.edu.vn': laurie
Password for 'http://laurie@gitlab-students.int.intek.edu.vn':
warning: redirecting to http://gitlab-students.int.intek.edu.vn/training-day-00/laurie.git/
remote: Counting objects: 9, done.
remote: Compressing objects: 100% (3/3), done.
remote: Total 9 (delta 0), reused 0 (delta 0)
Unpacking objects: 100% (9/9), done.

# Go into the directory you just cloned
$ cd day00

# Make a folder that has the same name as the first exercice
$ mkdir hello_world

# Here you solve the exercice!

# Check that the folder has the file requested in the subject, and only that one
$ ls hello_world/ 
greetings

# Allow git to register your file
$ git add hello_world/ 

# Check that your file is staged by git
$ git status
On branch master

No commits yet

Changes to be committed:
  (use "git rm --cached ..." to unstage)

	new file:   hello_world/greetings

# Commit your first exercice
$ git commit -m "exercice hello_world"
[master (root-commit) e452b08] exercice hello_world
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 hello_world/greetings
 
# Push your work on the remote Git repository to save it and make it available for the Sentinel
$ git push origin master

# Onto the second exercice!

```
---
## hello_world
#### Notions: managing files

### Your mission
Welcome to your first day at INTEK - the International Network of Trusted Espionage Kin.

Your first mission is to create a file called `greetings`, containing only the following two words: `Hello World`

### Example
When testing that file with the commands `wc` and `cat`, you should get the following output:
```sh
$ cat greetings
Hello World
$ wc -l greetings
     0 greetings
$ cat -e greetings
Hello World$
```

### Tips
How to solve this exercice? First, you have to look at the examples above. Look up `man wc` and `man cat` (and the options from the examples) to understand what is going on, and why you get those outputs.

You must get the exact same outputs when you run those commands on your file `greetings`.

Finally, make sure your file contains the exact words and that the file has the correct name as well.

### Don't forget!
Each exercice must be submitted in a directory named after the exercice.

That means for this first exercice, you must create a directory called `hello_world`, and put your file `greetings` inside that directory.

---

## greet
#### Notions: permissions and shell script

### Your mission
Your next mission is to write a script named `greet`. The script will greet the world with the famous sentence `Hello world`

### Example
```sh
$ ./greet
Hello world
$

```

### Tips
The e-learning will tell you how to write a shell script. There's no trap here, you just have to write a small shell script that outputs the sentence `Hello world`.

---

## make_folders
#### Notions: permissions, directories, and shell script

### Your mission
Let's now prepare your working environment and its security clearance.

You will write a script `make_folders` that creates 3 folders with specific permissions:
- `public` readable, writable and executable by the owner and the owner's group; only readable and executable by others
- `profile` readable, writable, and executable by the owner; readable and executable by owner's group; no permissions for others
- `missions` readable and executable by the owner; no permissions for the owner's group and others

### Example
```sh
$ ls -l
total 0
-rwxr-xr-x  1 daniel  staff  0 Jan 31 10:57 make_folders
$ ./make_folders
$ ls -l
total 0
-rwxr-xr-x  1 daniel  staff   0 Jan 31 10:57 make_folders
dr-x------  2 daniel  staff  64 Jan 31 10:58 missions
drwxr-x---  2 daniel  staff  64 Jan 31 10:58 profile
drwxrwxr-x  2 daniel  staff  64 Jan 31 10:58 public

```

### Tips
To solve this exercice, you need to know how to create an empty directory and how to handle file permissions. It's in the e-learning, look it up!

And remember that you're only submitting a script that creates those folders. Don't submit the directories themselves.

---

## spy_library
#### Notions: file processing, pipes, and shell script

### Your mission
Your boss wants you to get a list of books about spying, but he doesn't want any book about the famous Mata Hari.

Your mission is to write a script `spy_library`. The script prints the titles from the file `books` that have "spy" in them, but not "mata" or "hari".

### Example
Here is an example of what `books` might contain and what your script should return.
```sh
$ cat books
The Billion Dollar Spy
MATA HARI: Dancer, Courtesan, Spy
THE SPY
The great Mata spy
A legacy of spies
Survive like a spy
Spymaster
The spy: a novel of mata hari
Hari's Last Spy Dance
$ ./spy_library
The Billion Dollar Spy
THE SPY
Survive like a spy
Spymaster

```

### Tips
You need two things to solve this exercice:

- understand how to use pipes (go ahead, look it up in the e-learning)
- `man grep`

---

## where_am_i
#### Notions: file processing and pipes

### Your mission
As part of your mission for INTEK you have to infiltrate the evil Pancake Corp's network. Getting access on their servers is not complicated, they always use the same password.

To prepare your infiltration, write a script `where_am_i`. The script tells the current directory's name without the parent directories.

### Example
```sh
$ pwd
/home/lou/hack_pancake_evil_corp/scripts

$ ./where_am_i
scripts

```

### Tips
All the commands you need, for this exercice and the next ones, are in the "file processing" chapter of the e-learning.

You should also test your script in different folders (like `/tmp`, `/home/laurie/Hyperspace`...) to be sure it works properly.

---

## count_double_agents
#### Notions: file processing

### Your mission
This morning, someone removed from a secret file some hashes that might give information about double agents. Thankfully, we still have yesterday's backup.

By counting the number of lines that have been removed, you can deduce the number of double agents.

Your mission is to write a script `count_double_agents` that prints the number of removed lines. The files to compare are `secret-yesterday` and `secret-today`.

### Example
Here are examples of what `secret-yesterday` and `secret-today` might contain, and the expected output with those examples.
```sh
$ cat secret-yesterday
ca12b24dc0d30c9627076cd08f281cdb6aeafce88964d611387a84db8467759e
39e26e6f2745a455c51f8fabfbd34997cc985b33d105a63f852c4a7305d956db
dfe64b282826acdcee215ea0104d53d8a8d49501319e0d2a1861b426b6d8b367
1fea61791f48b4bbf660b94d35bf5a772e54974a3593addb2257a5cddb2755c5
1eab7f6c61ffb1e64faf28ba111358e107bc45350e9a9f61f15ae8442d3118ef
c5f9cfc040e472533b85ee6d83bf8cb5e6fe9056211884035039c622d13fe963
6228f22d44ddf0eb5bac7b604e9d691b5f626db0b500576185509891e8e3a195

$ cat secret-today
ca12b24dc0d30c9627076cd08f281cdb6aeafce88964d611387a84db8467759e
39e26e6f2745a455c51f8fabfbd34997cc985b33d105a63f852c4a7305d956db
1fea61791f48b4bbf660b94d35bf5a772e54974a3593addb2257a5cddb2755c5
6228f22d44ddf0eb5bac7b604e9d691b5f626db0b500576185509891e8e3a195

$ ./count_double_agents
3

```

---

## find_password
#### Notions: file processing and pipes

### Your mission
A secret agent sent a file with the mentors' passwords. Write a script `find_password` to get mathieu's password.

The file's name is `passwords`.

### Example
Here is an example of what `passwords` might contain, and the expected output with that example.
```sh
$ cat passwords
daniel:ILoveCats42
laurie:MeowMeow<3<3
lou:WassupBro
mathieu:BecauseImWorthIt1337
toan:BringItOn
$ ./find_password
BecauseImWorthIt1337

```
---

## find_traitor
#### Notions: file processing

### Your mission
We got intel on who was leaking information. Our sources sent us a file called `suspects`. By sorting the file alphabetically, the second-to-last line contains information about the traitor.

Write a script `find_traitor` to find the name of the mole.

### Example
Here is an example of what `suspects` might contain, and the expected output with that example.
```sh
$ cat suspects
jboud:25:Jean Boudin
bbardot:57:Brigitte Bardot
alourd:34:Alain Lourd
mcantal:62:Marcel Cantal
$ ./find_traitor
Jean Boudin

```
---

## find_info
#### Notions: file processing, file listing, and pipes

### Your mission
One of our agents managed to get sensitive information about the traitor inside INTEK, but she has gone missing and it's up to you to find that information on her laptop. Thankfully, you know that the info is hidden in the smallest file whose name contains at least one vowel.

Your mission is to write a script `find_info`. It will print all files (including hidden ones) contained in the current directory, but showing only the files where the title contains a vowel. And finally they need to be sorted with the smallest file first.

Those informations are critical, we're counting on you young recruit!

### Tips
Take a careful look at the example to know what we consider a vowel.

### Example
```sh
$ ls .
[          cp         df         expr       launchctl  mkdir      pwd        sh         tcsh       zsh
bash       csh        domainname find_info    link       mv         rcp        sleep      test
cat        date       echo       kill       ln         pax        rm         stty       unlink
chmod      dd         ed         ksh        ls         ps         rmdir      sync       wait4path

$ ./find_info
sleep
find_info
echo
rmdir
wait4path
domainname
mkdir
kill
link
test
expr
cat
unlink
date
chmod
ed
pax
launchctl
bash

```
---

## what_is_your_20
#### Notions: advanced file processing

### Your mission
You have access to the unsecure servers of the evil Pancake Corp's network. Write a script `what_is_your_20` that will get the vulnerable server's ip. We will use the ip later on to map Pancake Corp's servers.

Make sure you return only the ip of the ethernet.

### Example
```sh
$ ifconfig
lo0: flags=8049<UP,LOOPBACK,RUNNING,MULTICAST> mtu 16384
        options=1203<RXCSUM,TXCSUM,TXSTATUS,SW_TIMESTAMP>
        inet addr:127.0.0.1 netmask 0xff000000
        inet6 addr:::1 prefixlen 128
        inet6 addr:fe80::1%lo0 prefixlen 64 scopeid 0x1
        inet addr:127.94.0.1 netmask 0xff000000
        nd6 options=201<PERFORMNUD,DAD>
gif0: flags=8010<POINTOPOINT,MULTICAST> mtu 1280
stf0: flags=0<> mtu 1280
eth0: flags=8863<UP,BROADCAST,SMART,RUNNING,SIMPLEX,MULTICAST> mtu 1500
        ether 78:31:c1:c9:5a:54
        inet6 addr:fe80::1040:1564:1cc9:35ea%en0 prefixlen 64 secured scopeid 0x4
        inet addr:192.168.1.29 netmask 0xffffff00 broadcast 192.168.10.255
        nd6 options=201<PERFORMNUD,DAD>
        media: autoselect
        status: active
eth1: flags=963<UP,BROADCAST,SMART,RUNNING,PROMISC,SIMPLEX> mtu 1500
        options=60<TSO4,TSO6>
        ether 72:00:02:6d:05:70
        media: autoselect <full-duplex>
        status: inactive
eth2: flags=963<UP,BROADCAST,SMART,RUNNING,PROMISC,SIMPLEX> mtu 1500
        options=60<TSO4,TSO6>
        ether 72:00:02:6d:05:71
        media: autoselect <full-duplex>
        status: inactive
bridge0: flags=8863<UP,BROADCAST,SMART,RUNNING,SIMPLEX,MULTICAST> mtu 1500
        options=63<RXCSUM,TXCSUM,TSO4,TSO6>
        ether 72:00:02:6d:05:70
        Configuration:
                id 0:0:0:0:0:0 priority 0 hellotime 0 fwddelay 0
                maxage 0 holdcnt 0 proto stp maxaddr 100 timeout 1200
                root id 0:0:0:0:0:0 priority 0 ifcost 0 port 0
                ipfilter disabled flags 0x2
        member: en1 flags=3<LEARNING,DISCOVER>
                                        ifmaxaddr 0 port 5 priority 0 path cost 0
        member: en2 flags=3<LEARNING,DISCOVER>
                                        ifmaxaddr 0 port 6 priority 0 path cost 0
        nd6 options=201<PERFORMNUD,DAD>
        media: <unknown type>
        status: inactive
p2p0: flags=8843<UP,BROADCAST,RUNNING,SIMPLEX,MULTICAST> mtu 2304
        ether 0a:31:c1:c9:5a:54
        media: autoselect
        status: inactive
awdl0: flags=8943<UP,BROADCAST,RUNNING,PROMISC,SIMPLEX,MULTICAST> mtu 1484
        ether d2:d6:b2:ea:3e:0a
        inet6 addr:fe80::d0d6:b2ff:feea:3e0a%awdl0 prefixlen 64 scopeid 0x9
        nd6 options=201<PERFORMNUD,DAD>
        media: autoselect
        status: active
utun0: flags=8051<UP,POINTOPOINT,RUNNING,MULTICAST> mtu 2000
        inet6 addr:fe80::69b3:4d3c:6c1c:e54e%utun0 prefixlen 64 scopeid 0xa
        nd6 options=201<PERFORMNUD,DAD>
        
$ ./what_is_your_20
192.168.1.29

```
---

## intruders
#### Notions: advanced file processing

### Your mission
Our beloved INTEK is being attacked every night. We're suspecting the evil Pancake Corp to be behind this very cheap DDOS attack. We need some data to confirm our suspicion.

Your mission is to write a script named `intruders` that:

- reads the file `access.log`
- prints, for each IP address, the number of connections from 18:00 to 05:00
- sorts from the fewer connections to higher connections.
If two IPs have the same number of connections, sort them numerically from lower to higher. We consider that 10.0.0.1 < 10.0.0.2 < 10.0.1.1

The format of each line is:
```sh
n IP
```
with `n` being the number of total connections and `IP` being the IP address.

### Example
```sh
$ tail access.log
66.249.73.210 - - [29/Jan/2018:04:08:53 +0100] "GET /robots.txt HTTP/1.1" 404 693 "-" "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)"
66.102.7.245 - - [29/Jan/2018:04:08:53 +0100] "GET /users/sign_in HTTP/1.1" 200 1684 "-" "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon"
66.249.73.210 - - [29/Jan/2018:04:08:53 +0100] "GET /users/sign_in HTTP/1.1" 200 1684 "-" "Googlebot-Image/1.0"
66.102.7.241 - - [29/Jan/2018:04:08:54 +0100] "GET /users/sign_in HTTP/1.1" 200 1679 "http://www.google.com/search" "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko; Google Web Preview) Chrome/41.0.2272.118 Safari/537.36"
66.102.7.241 - - [29/Jan/2018:04:08:54 +0100] "GET /favicon.ico HTTP/1.1" 200 0 "-" "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon"
66.102.7.245 - - [29/Jan/2018:04:08:54 +0100] "GET /assets/logo-843035499f7a80775d887b083d2f1a1f.png HTTP/1.1" 200 17012 "https://intek.io/users/sign_in" "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko; Google Web Preview) Chrome/41.0.2272.118 Safari/537.36"
66.102.7.243 - - [29/Jan/2018:04:08:54 +0100] "GET /images/uk.png HTTP/1.1" 200 12173 "https://intek.io/users/sign_in" "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko; Google Web Preview) Chrome/41.0.2272.118 Safari/537.36"
66.102.7.241 - - [29/Jan/2018:04:08:54 +0100] "GET /assets/application-9a2f8428b6b0135a2a253094a3c1ebd8.css HTTP/1.1" 200 274545 "https://intek.io/users/sign_in" "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko; Google Web Preview) Chrome/41.0.2272.118 Safari/537.36"
66.102.7.241 - - [29/Jan/2018:04:08:54 +0100] "GET /assets/application-d9f90cb324f611e5c3eb5e8d9f518a29.js HTTP/1.1" 200 518584 "https://intek.io/users/sign_in" "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko; Google Web Preview) Chrome/41.0.2272.118 Safari/537.36"
66.102.7.241 - - [29/Jan/2018:04:08:55 +0100] "GET /users/sign_in HTTP/1.1" 200 1682 "https://intek.io/users/sign_in" "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko; Google Web Preview) Chrome/41.0.2272.118 Safari/537.36"

$ ./intruders
5 104.233.73.171
5 184.162.160.33
61 91.200.12.64
72 5.194.136.89
90 77.207.191.133
11000 1.46.139.151

```

Take the file `access.log` in repository.
---